#ifndef ASSEMBLY_H
#define ASSEMBLY_H

#include"../Include/SymbolList.h"

SymbolList* SymList;

// Instruction types
#define INST_A 0b0000000000000000
#define INST_C 0b1110000000000000

//Sources
#define SOURCE_A 0b0000000000000
#define SOURCE_M 0b1000000000000

// Jumps
#define CASE_JGT 0b001
#define CASE_JE  0b010
#define CASE_JGE 0b011
#define CASE_JLT 0b100
#define CASE_JNE 0b101
#define CASE_JLE 0b110
#define CASE_JMP 0b111

// Destinations
#define DEST_M   0b001000
#define DEST_D   0b010000
#define DEST_MD  0b011000
#define DEST_A   0b100000
#define DEST_AM  0b101000
#define DEST_AD  0b110000
#define DEST_AMD 0b111000

// Operation Code
#define OP_0      0b1010100000000
#define OP_1      0b1111110000000
#define OP_MINUS1 011101000000000
#define OP_D      0b0011010000000
#define OP_AM     0b1100000000000
#define OP_NOTAM  0b1100010000000
#define OP_DSUB1  0b1101110000000
#define OP_DADD1  0b0111110000000
#define OP_DSUBAM 0b0100110000000
#define OP_DADDAM 0b0000100000000

char* TranslateToMachineLang(char* list, int type);

#endif
