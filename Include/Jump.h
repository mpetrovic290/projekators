#ifndef JUMP_H
#define JUMP_H

#define LIST_OF_JUMPS	  \
    X(JMP)		  \
    X(JE)		  \
    X(JNE)		  \
    X(JGT)		  \
    X(JGE)		  \
    X(JLT)		  \
    X(JLE)		  

enum
  {
   #define X(jump) jump,
   LIST_OF_JUMPS
   #undef X
  };

/* #define X(jump) int CASE##_##jump=0; */
/* LIST_OF_JUMPS */
/* #undef X */
     
#endif
