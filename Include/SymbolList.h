#ifndef SYMBOL_LIST_H
#define SYMBOL_LIST_H

typedef struct SymbolList SymbolList;

void AddSymbolToList(SymbolList **list, int LineNo, char* Instruction);

int GetSymbol(SymbolList *list, char* symbol);

#endif
