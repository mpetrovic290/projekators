#ifndef SYNTAXCHECKER_H
#define SYNTAXCHECKER_H

#include <stdio.h>

#define LIST_OF_STATES			\
    X(A_TRUE)				\
    X(D_TRUE)				\
    X(M_TRUE)				\
    X(JMP_TRUE)				\
    X(LBL_TRUE)			    

enum
{
   START=10,
   #define X(STATE) STATE,
   LIST_OF_STATES
   #undef X
};

void ClearWhiteSpacesAndComments(char** line);

int CheckSyntax(char* line, size_t LineNo);

#endif
