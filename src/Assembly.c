#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "../Include/Assembly.h"
#include "../Include/SyntaxChecker.h"
#include "../Include/Jump.h"
#include "../Include/SymbolList.h"

int Address=0b0000000000010000;

int CheckMD(char* line)
{
  short Code=0;
  
  if(strcmp(line,"D")==0)
    Code|=OP_D;
  else if(strcmp(line,"A")==0)
    Code|=OP_AM;
  else if(strcmp(line,"D-1")==0)
    Code|=OP_DSUB1;
  else if(strcmp(line,"D+1")==0)
    Code|=OP_DADD1;
  else if(strcmp(line,"D+M")==0 || strcmp(line,"D+A")==0)
    Code|=OP_DADDAM;
  else if(strcmp(line,"D-M")==0 || strcmp(line,"D-A")==0)
    Code|=OP_DSUBAM;
  else if(strcmp(line,"0")==0)
    Code|=OP_0;
  else if(strcmp(line,"1")==0)
    Code|=OP_1;
  else if(strcmp(line,"-1")==0)
    Code|=OP_MINUS1;

  return Code;
}

char* printBits(size_t const size, void const * const ptr)
{
  unsigned char *b = (unsigned char*) ptr;
  unsigned char byte;
  char* line=(char*)malloc(16*sizeof(char));
  char* temp=line;
  int i, j;

  for (i=size-1;i>=0;i--)
    {
      for (j=7;j>=0;j--)
        {
	  byte = (b[i] >> j) & 1;
	  *temp++=byte+'0';
        }
    }
  return line;
}
char* TranslateToMachineLang(char* line, int type)
{
  short Code;
  char* MachineCode=(char*)malloc(255*sizeof(char));

  (type==A_TRUE) ? (Code=INST_A) : (Code=INST_C);
  
  switch(type)
    {     
    case A_TRUE:
      if(line[1]=='R')
	Code|=line[2]-'0';     
      else if(isdigit(line[1]))
	{
	  line++;
	  Code|=atoi(line);	  
	}
      else if((Code|=GetSymbol(SymList,line+1))!=EXIT_FAILURE)
	break;
      else
	{
	  Code|=Address;
	  AddSymbolToList(&SymList, Address++, line);
	}	
      break;
      
    case D_TRUE:
      Code|=DEST_D;
      line+=2;
      Code|=CheckMD(line);
      
      break;
      
    case M_TRUE:
      Code|=DEST_M;
      if(line[1]=='D')
	{
	  Code|=DEST_D;
	  line+=3;
	}
      else
	line+=2;
      Code|=CheckMD(line);
      
      break;
      
    case LBL_TRUE:
      return "";
      break;
      
    case JMP_TRUE:
      if(line[0]=='D')
	Code|=DEST_D;
      line+=2;
      #define X(jump) if(strcmp(line,""#jump)==0) Code|=CASE##_##jump;
      LIST_OF_JUMPS
      #undef X
      break;
      
    default:
      break;
    }

  MachineCode=printBits(sizeof(Code), &Code);
  return MachineCode;
}
