#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../Include/I_O.h"
#include "../Include/SyntaxChecker.h"
#include "../Include/SymbolList.h"
#include "../Include/Assembly.h"

#define MAX_NUMBER_OF_CHARS 256
#define MAX_NUMBER_OF_LINES 512

void ParseFile(char* FileLocation)
{
  SymList=NULL;

  size_t LineNo=1;
  int type[MAX_NUMBER_OF_LINES];

  FILE *fp;
  
  char *buff=(char*)malloc(MAX_NUMBER_OF_CHARS*sizeof(char));
  char *translatedBuff=(char*)malloc(MAX_NUMBER_OF_CHARS*sizeof(char));
  char program[MAX_NUMBER_OF_CHARS][MAX_NUMBER_OF_LINES];  

  fp = fopen(FileLocation, "r");
  
  while(fgets(buff, MAX_NUMBER_OF_CHARS, (FILE*)fp)!=NULL)
    {
      ClearWhiteSpacesAndComments(&buff);

      if(strcmp(buff,"")==0)
	continue;
            
      if((type[LineNo]=CheckSyntax(buff,LineNo))==EXIT_FAILURE)
	return;      
      
      if(type[LineNo]==LBL_TRUE)
	AddSymbolToList(&SymList, LineNo, buff);
      
      strcpy(program[LineNo++],buff);
    }
  
  fclose(fp);

  for(size_t i=1;i<LineNo;i++)
    {
      translatedBuff=TranslateToMachineLang(program[i], type[i]);

      printf("%s\t\t\t%s\n",program[i],translatedBuff);
    }
}
