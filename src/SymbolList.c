#include"../Include/SymbolList.h"
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>

struct SymbolList
{
  int value;
  char* Symbol;
  SymbolList* next;
};

void CleanSymbol(char** line)
{
  if(**line=='(')
    {
      (*line)++;
      strncpy(*line,*line,strlen(*line)-1);
    }
  else
    (*line)++;
}

int GetSymbol(SymbolList *list, char* symbol)
{  
  while (list!=NULL && strcmp(list->Symbol,symbol)!=0)
    {
      //printf("%s\t\t%s\n",list->Symbol, symbol);
      list=list->next;
    }
    

  if(list==NULL)
    return EXIT_FAILURE;
  else
    return list->value;
}

void AddSymbolToList(SymbolList **list, int value, char* Symbol)
{
  char* CleanSymbol=(char*)malloc(255*sizeof(char));

  if(*Symbol=='(')
    {
      Symbol++;
      strncpy(CleanSymbol,Symbol,strlen(Symbol)-1);      
    }
  else
    CleanSymbol=++(Symbol);
  
  SymbolList *new_node = malloc(sizeof(SymbolList));
  
  if (new_node!=NULL)
    {      
      new_node->value = value;
      new_node->Symbol = CleanSymbol;
      new_node->next = NULL;

      while ( *list!=NULL )
	list=&(*list)->next;

      *list=new_node;
    } 
}


