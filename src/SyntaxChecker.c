#include "../Include/SyntaxChecker.h"
#include "../Include/Jump.h"
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

void ClearWhiteSpacesAndComments(char** line)  
{
  char *buff=malloc(255*sizeof(char));
  int i=0;
  
  while(**line!='\0')
    {
      if(**line=='/')
	if(strncmp("//", *line, 2)==0)
	  break;
      
      if(!isspace(**line) && **line!='\t')
	buff[i++]=**line;
            
      (*line)++;
    }
  
  buff[i]='\0';
  
  *line=buff;
}

int CheckAOperation(char* line)
{
  char* buff=line;
  
  if (*buff++=='@')
    {
      if(isalpha(*buff))
	return A_TRUE;
      
      else if(isdigit(*(buff++)))
	{
	  while(*buff!='\0')
	    {
	      if(!isdigit(*buff++))
		return EXIT_FAILURE;
	    }
	  return A_TRUE;
	}	          
    }

  return EXIT_FAILURE;
}

int CheckDOperation(char* line)
{
  char* buff=line;
  
  if (*(buff+1)!=';' && *buff++=='D')
    {      
      if(*buff++!='=')
	return EXIT_FAILURE;

      if(!(*buff=='M' || *buff=='D' || *buff=='A' || isdigit(*buff)))
	return EXIT_FAILURE;

      buff++;

      if(*buff=='\0')
	return D_TRUE;

      if(!(*buff=='+' || *buff=='-'))
	return EXIT_FAILURE;

      buff++;

      if(*buff=='M' || *buff=='D' || *buff=='A' || isdigit(*buff))
	return D_TRUE;
    }

  return EXIT_FAILURE;
}


int CheckMOperation(char* line)
{
  char* buff=line;
  
  if (*buff++=='M')    
    {
      if(*buff=='D')
	if(CheckDOperation(buff)==D_TRUE)
	  return M_TRUE;
	  
      if(*buff++!='=')
	return EXIT_FAILURE;

      if(*buff=='M' || *buff=='D' || *buff=='A' || isdigit(*buff))
	return M_TRUE;           
    }
  
  return EXIT_FAILURE;
}

int CheckJMP(char* line)
{
  char* buff=line;
  
  if (*buff=='0' || *buff=='D')
    {
      buff++;
      
      if(*buff++!=';')
	return EXIT_FAILURE;

      #define X(jump) if(strcmp(#jump"",buff)==0) return JMP_TRUE;
      LIST_OF_JUMPS
      #undef X      
    }
  return EXIT_FAILURE;   
}

int CheckLabel(char* line)
{
  char* buff=line;
  
  if(*buff++=='(')
    {
      while(isalpha(*buff) || *buff=='_')
	buff++;

      if(*buff==')')
	return LBL_TRUE;           
    }
  return EXIT_FAILURE;
}

int CheckSyntax(char* line, size_t LineNo)
{
  int out;
  switch(*line)
    {    
    case '@':
      if((out=CheckAOperation(line))==EXIT_FAILURE)
	  printf("Syntax error on line %zu: Bad A Operation: %s!\n",LineNo , line);
      break;
      
    case 'D':
      if(*(line+1)==';')
	{
	  if((out=CheckJMP(line))==EXIT_FAILURE)
	      printf("Syntax error on line %zu: Bad Jump Operation: %s!\n",LineNo , line);
	  break;
	}      
      if((out=CheckDOperation(line))==EXIT_FAILURE)
	  printf("Syntax error on line %zu: Bad D Operation: %s\n!",LineNo , line);
      break;

    case '0':    
      if((out=CheckJMP(line))==EXIT_FAILURE)
	  printf("Syntax error on line %zu: Bad Jump Operation: %s!\n",LineNo , line);
      break;
      
    case 'M':
      if((out=CheckMOperation(line))==EXIT_FAILURE)
	  printf("Syntax error on line %zu: Bad M Operation: %s\n!",LineNo , line);
      break;

    case '\0':
      out=EXIT_SUCCESS;
      break;
      
    case '(':
      if((out=CheckLabel(line))==EXIT_FAILURE)
	  printf("Syntax error on line %zu: Bad Label: %s!\n",LineNo , line);           
      break;      

    default:
      printf("Syntax error on line %zu:  %s!\n",LineNo , line);      
      out=EXIT_FAILURE;
    }
  
  return out;
}
